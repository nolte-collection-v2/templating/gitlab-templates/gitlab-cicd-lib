# Shared Lib For GitlabCI

Shared Lib for reuseable GitLab CI/CD resources.

## Usage

```yaml
include:
  - project: 'nolte-collection/templating/gitlab-ci'  
    ref: 'master'
    file: 'ci-templates/tf-basement.yml'
  - project: 'nolte-collection/templating/gitlab-ci'  
    ref: 'master'
    file: 'ci-templates/gl-releasing.yml'
```


## Used Tools

* [semantic-release/semantic-release](https://github.com/semantic-release/semantic-release)


### Releasing

For reasing using some commit message type like:

```
fix(docs): Add more details to the Readme docs
```